
#!/bin/bash

apt-get install netcat -y


while ! nc -z ${DB_HOST} 3306 ;
do
sleep 1;
done
apt-get install net-tools -y
service apache2 start
service php-fpm start

ip_adress_in_net1=$(ifconfig eth0 | grep "inet" | awk '{print $2}');
mysql -u $DB_ROOT --password=${MARIADB_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${DOLIBARR_DB_NAME};" --host=database;
mysql -u $DB_ROOT --password=${MARIADB_ROOT_PASSWORD} -e "CREATE USER IF NOT EXISTS '${DOLIBARR_USER}'@'${ip_adress_in_net1}' IDENTIFIED BY '${DOLIBARR_PWD}';" --host=database;
mysql -u $DB_ROOT --password=${MARIADB_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON ${DOLIBARR_DB_NAME}.* TO '${DOLIBARR_USER}'@'${ip_adress_in_net1}';" --host=database;

# /usr/sbin/apache2ctl -D FOREGROUND
exec "$@"