# #!/bin/bash

[[ -z "${DOLIBARR_USER}" ]] && dolibarr_user='dolibarruser' || dolibarr_user="${DOLIBARR_USER}"
[[ -z "${DOLIBARR_PWD}" ]] && dolibarr_pwd='password' || dolibarr_pwd="${DOLIBARR_PWD}"
[[ -z "${DOLIBARR_DB_NAME}" ]] && dolibarr_db_name='dolibarr' || dolibarr_db_name="${DOLIBARR_DB_NAME}"

mysql -u $DB_ROOT -p -e "CREATE DATABASE IF NOT EXISTS dolibarr";
 
echo "CREATE DATABASE IF NOT EXISTS users;
CREATE DATABASE IF NOT EXISTS dolibarr;
CREATE USER IF NOT EXISTS dolibarruser@localhost IDENTIFIED BY password;
GRANT ALL PRIVILEGES ON dolibarr.* TO dolibarruser@localhost; 
CREATE USER IF NOT EXISTS keycloak@localhost IDENTIFIED BY keycloak;
GRANT ALL PRIVILEGES ON users.* TO keycloak@localhost;" > "/docker-entrypoint-initdb.d/init.sql";