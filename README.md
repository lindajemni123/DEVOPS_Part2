# DEVOPS_Part2

1- Préparation des images: 

    Dans cette partie on a divisé le travail sur 4 taches principales: 
    
        1.1- configuration du Debian Buster 64 bits qui doit intégré Apache2 et PHP 7 et les modules nécessaires à 
        Dolibarr                
            - download et build code source de php https://github.com/kasparsd/php-7-debian.git
            - liaison php dolibarr via le fichier de config web/php-conf/php-dolibarr.conf
        1.2- Configuration d'un Container Mariadb indépendant pour faire fonctionner Dolibarr
            - Récupération de l'image mariaDB
            - mise en place des scripts entrypoint afin de permettre l'instace web dolibbar de se connecter avec mariaDb et de creer son utilisateur avec les privilèges sur la base de donnée dolibarr.
        
        1.3- Lancer les instaces avec docker-compsoe
                - Ecriture ./docker-compose.yml qui permet de lancer les deux services dolibarr & mariadb
                - Ajout des volumes pour la base de donner et dolibarr.
                - Ajout d'un Bridge qui met en liaison les deux services.
                - Attribution d'un fichier de Env pour chaque service
                - Lancer le petit cluster en local.
                - Tester le bon fonctionnement de Dolibarr sur Localhost.(Localhost:80)

        1.4- Push les images dans dockerHub.
                - Nos images sont sous dockerhub https://hub.docker.com/repository/docker/ljemni/devops_part2
        
Pour tester cette partie, il suffit de :
- Cloner le projet en local 
- Lancer la commande  : docker-compose build ./docker-compose.yml
- Lancer le docker-compose : docker-compose up


2- Préparation de l’infrastructure : 

    Sur AWS : 
        - création d'un IAM user en lui donnant les droit d'acces à la console "devops_user"
        - Un referentiels ECR pour lui transférer les images que nous devons déployer sur notre cluster EKS.
        - création d'un reseau VPC avec la confirations de: 
            - 3 sous reseaux 
            - table de routage 
            - passerelle internet 
            - groupe de sécurité 

        - création des IAM roles pour la configuration du groupe des neouds 
            - creation d'un role de service EC2 et l'attributions des autorisations suivantes qui permetteront aux instances EC2 de créer le nœud pour communiquer avec AWS, d'extraire des images d'ECR et d'attribuer la bonne adresse IP dans la configuration du VPC et du sous-réseau: 
                - AmazonEKSWorkerNodePolicy
                - AmazonEC2ContainerRegistryReadOnly
                - AmazonEKS_CNI_Politique


    Aprés avoir configuré nos paramétres réseaux on est passé à la creation de notre cluster "eks-cluster". 

    Le cluster doit etre crée en lui attribuant un EKS ROLE pour permettre l'interaction avec les nœuds et la configuration interne de leurs pods

    Une fois le cluster crée on passe à la création du groupe de noeuds "node_goup".  c'est un groupe de services EC2.

    Ce groupe contiendra nos pods et la configuration de base pour gérer la mise en réseau et communiquer avec tous les services AWS tels que le VPC.

    Notre groupe de noeuds contient 4 noeuds.

3- Déploiement: 

    Puisque nous avons créé toutes les ressources nécessaires dans notre compte AWS, nous passons continuer à la configuration locale ainsi que les fichiers de déploiement pour notre cluster EKS. 
    Nous avons commencé par :
        - Connecter Kubectl à notre cluster: 
            - installer kubectl 
            - installer eksctl 
            - installer aws-iam-authenticator
            - aws sts get-caller-identity: cette commande réponds avec le compte que nos avons défini sur notre application aws-cli.
            {
                "UserId": "AIDATJ7BRO725DDUG4YWK",
                "Account": "227569399797",
                "Arn": "arn:aws:iam::227569399797:user/devops_user"
            }
            - aws eks --region us-east-2 update-kubeconfig --name eks-cluster Cette commande configurera notre fichier kubectl pour se connecter à AWS. On peut voir la config modifié sur  ~/.kubectl/config

            - kubectl get nodes -o wide : cette commande nous confirme que notre machine est connecté aux cluster: elle listera nos 3 noeuds: 

    NAME                                       STATUS   ROLES    AGE    VERSION               INTERNAL-IP   EXTERNAL-IP      OS-IMAGE         KERNEL-VERSION                CONTAINER-RUNTIME
    ip-10-0-0-224.us-east-2.compute.internal   Ready    <none>   3d7h   v1.21.2-eks-55daa9d   10.0.0.224    3.144.132.54     Amazon Linux 2   5.4.141-67.229.amzn2.x86_64   docker://19.3.13
    ip-10-0-1-140.us-east-2.compute.internal   Ready    <none>   3d7h   v1.21.2-eks-55daa9d   10.0.1.140    18.224.141.242   Amazon Linux 2   5.4.141-67.229.amzn2.x86_64   docker://19.3.13
    ip-10-0-2-187.us-east-2.compute.internal   Ready    <none>   3d7h   v1.21.2-eks-55daa9d   10.0.2.187    3.141.27.86      Amazon Linux 2   5.4.141-67.229.amzn2.x86_64   docker://19.3.13

        - Créer le dossier kube qui contient nos fichiers de deploiement, services et PVC: 
            - dolibarr-ressorces.yaml
            - mariadb-ressources.yaml 
        - deployer en executant : kubectl apply -f ./kube:
            - Cette commande synchronisera les instructions dans les fichiers de déploiement avec EKS et tous les pods commenceront à être créés et initialisés.
        une fois le deploiement terminé on aura nos 3 pods : 

                                        dolibarr-8686c7dfc9-sb9p6   1/1     Running   0          42h
                                        dolibarr-8686c7dfc9-xm2rv   1/1     Running   0          42h
                                        mariadb-5b8d6645b8-wknbw    1/1     Running   1          42h
            
            - La commande : "kubectl get services" répondra avec tous les services, y compris l'URL dans la colonne IP externe: 

    NAME         TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)        AGE
    dolibarr     LoadBalancer   172.20.153.155   a414e85ab704f4d56b002eec0dc3cad0-1465447364.us-east-2.elb.amazonaws.com   80:30286/TCP   42h
    kubernetes   ClusterIP      172.20.0.1       <none>                                                                    443/TCP        3d8h
    mariadb      ClusterIP      172.20.71.172    <none>                                                                    3306/TCP       42h

Maintenant, lorsque nous visitons l'URL : "a414e85ab704f4d56b002eec0dc3cad0-1465447364.us-east-2.elb.amazonaws.com ", notre service est opérationnel.

4- Déploiement continu: 

    4.1- configuration du processus du build et de déploy dans gitlab ( gitlab-ci )

    - Push le projet dans un repository gitLab
    - Ecrire le script .gitlab-ci qui décrit le hook sur la branche principal & configuration du build et de deploy 
    - Le script s'excute dans le worker Gitlab.
    - Tester si à chaque push le rebuild des images est bien fait et des les images d'une nouvelle version est 
        deployé sur le cluster aws.

Pour tester cette partie il faut :
- Push le projet sur GitLab
- Créer les crédentiels de votre cluster aws sur GitLab.
- Verifier que tout les varriables dans le fichier .gitlab-ci.yml sont définis sur votre repo.
- Le .gitlab-ci va lancer le build et le déploiement 
 Et vous voilà !!

5- Lier le cluster à outil de provisioning Runcher

    5.1- Create une instance rancher dans le sous réseau du cluster aws.
    
    - Creation de l'instance t2.meduim dans le sous réseau du cluster aws
    - Affecter une api elastic to à l'instance.
    - Installation rancher via image docker Rancher/Rancher:V2.6 : TO launch rancher 
        `sudo docker run --privileged -v /opt/rancher:/var/li --restart=unless-stopped --name rancher --hostname 3.132.38.9 -p 80:80 -p 443:443 rancher/rancher:v2.6-aeeaeb70ed778db354bc4e5736c396c2e11db713-linux-amd64`
    - Configuration de rancher : Import existing Cluster EKS amazon.
    - Tester le bon fonctionnement de rancher sur l'api elastic.
    - Visualisation de l'état du cluster et ses ressources.

 
